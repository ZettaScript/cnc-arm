#!/usr/bin/env python3

import math

class Arm:
	def __init__(self, a, b):
		self.a = a
		self.b = b
	
	# Transforms (radius,height) coords to (alpha,beta) coords
	def rz_to_ab(self, r, z):
		d = math.sqrt(r**2 + z**2)
		if d != 0 and d <= self.a+self.b and r != 0 and d >= self.a-self.b:
			z1 = (self.a**2*z - self.b**2*z + r**2*z + z**3 + math.sqrt(r**2*(4*self.a**2*r**2 + 4*self.a**2*z**2 - (self.a**2 - self.b**2 + r**2 + z**2)**2)))/(2*(r**2 + z**2))
			r1 = math.sqrt(self.a**2 - z1**2)
			if abs(math.sqrt((r+r1)**2+(z-z1)**2) - self.b) < abs(math.sqrt((r-r1)**2+(z-z1)**2) - self.b):
				r1 = -r1
			
			return (math.atan2(z1, r1), math.atan2(z-z1, r-r1))
	
	# Transforms (x,y,z) coords to (alpha,beta,theta) coords
	def xyz_to_abt(self, x, y, z):
		ab = self.rz_to_ab(math.sqrt(x**2 + y**2), z)
		if ab:
			return (*ab, math.atan2(y, x))
