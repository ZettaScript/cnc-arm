#!/usr/bin/env python3

import time, struct

UI8 = 1<<0 # unsigned char
UI16 = 1<<1 # unsigned short
UI24 = 1<<2
UI32 = 1<<3 # unsigned long
SI8 = 1<<4 # signed char
SI16 = 1<<5 # signed short
SI24 = 1<<6
SI32 = 1<<7 # signed long
SF32 = 1<<8 # signed float
SF64 = 1<<9 # signed double
BV = 1<<10 # string, ended with 0x00
BX8 = 1<<11 # string, fixed length UI8 (max 255)
BX16 = 1<<12 # string, fixed length UI16 (max 65535)
BX24 = 1<<13 # string, fixed length UI24 (max 16777215)
BX32 = 1<<14 # string, fixed length UI32 (max 4294967295)

"""
Commands for a 3-axis CNC driven by 32b Arduino
(only Arduino Due supports 64b floats, according to Arduino docs)
"""
commands_cnc32 = {
	"w": [b"\x10", [UI32]],
	"en": [b"\x11", [UI8]],
	"pos": [b"\x12", [UI8]],
	"home": [b"\x13", []],
	"x": [b"\x20", [UI16, SF32]],
	"y": [b"\x21", [UI16, SF32]],
	"z": [b"\x22", [UI16, SF32]],
	"xy": [b"\x23", [UI16, SF32, SF32]],
	"xz": [b"\x24", [UI16, SF32, SF32]],
	"yz": [b"\x25", [UI16, SF32, SF32]],
	"xyz": [b"\x26", [UI16, SF32, SF32, SF32]],
}

encode_arg = {
	UI8: lambda x: x.to_bytes(1, "little"),
	UI16: lambda x: x.to_bytes(2, "little"),
	UI24: lambda x: x.to_bytes(3, "little"),
	UI32: lambda x: x.to_bytes(4, "little"),
	SI8: lambda x: x.to_bytes(1, "little", signed=True),
	SI16: lambda x: x.to_bytes(2, "little", signed=True),
	SI24: lambda x: x.to_bytes(3, "little", signed=True),
	SI32: lambda x: x.to_bytes(4, "little", signed=True),
	SF32: lambda x: struct.pack("f", x),
	SF64: lambda x: struct.pack("d", x),
	BV: lambda x: x.encode()+b"\x00",
	BX8: lambda x: len(x).to_bytes(1, "little")+x.encode(),
	BX16: lambda x: len(x).to_bytes(2, "little")+x.encode(),
	BX24: lambda x: len(x).to_bytes(3, "little")+x.encode(),
	BX32: lambda x: len(x).to_bytes(4, "little")+x.encode()
}

def encode(commands, code):
	return b"".join(
		commands[line[0]][0] + b"".join(
			encode_arg[t](arg)
			for t, arg in zip(commands[line[0]][1], line[1])
		)
		for line in code
	)

if __name__ == "__main__":
	import sys
	
	if "--test" in sys.argv:
		import serial
		ser = serial.Serial()
		ser.baudrate = 9600
		ser.port = "/dev/ttyACM0"
		ser.open()
		time.sleep(2)
		ser.write(encode(commands_cnc32, [
			["en", [True]],
			["w", [100]],
			["x", [4000, 800]],
			["w", [100]],
			["en", [False]]
		]))
		"""ser.write(encode(commands_cnc32, [
			["en", [True]],
			["w", [100]],
			["x", [500, 200]],
			["x", [500, -200]],
			["y", [500, 200]],
			["y", [500, -200]],
			["z", [500, 200]],
			["z", [500, -200]],
			["w", [500]],
			#["xyz", [500, 100, 100, 1]],
			["xyz", [500, 50, -125, 300]],
			["w", [100]],
			["en", [False]]
		]))"""
		try:
			while True:
				sys.stdout.write(ser.read().decode())
				sys.stdout.flush()
		except KeyboardInterrupt:
			pass
		finally:
			ser.close()
