#!/usr/bin/env python3

import sys, math, time, pygame

BLACK = (0,0,0)
WHITE = (255,255,255)
GREY = (127,127,127)
RED = (255,0,0)

size = (640,480)
pygame.init()
speed = 60
info = pygame.display.Info()

flag = pygame.RESIZABLE
if "-f" in sys.argv:
	flag = pygame.FULLSCREEN
	size = (info.current_w, info.current_h)
screen = pygame.display.set_mode(size, flag|pygame.RESIZABLE)

pygame.mouse.set_visible(True)
pygame.display.set_caption("Arm coord test")

font1 = pygame.font.SysFont("LektonCode", 20)
clock = pygame.time.Clock()

done = False
resize = 0
sresize = None
xmid = int(size[0]/2)
ymid = int(size[1]/2)
a = min(size)/4
b = a

while not done:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			done = True
		elif event.type == pygame.VIDEORESIZE:
			resize = time.time()+0.2
			sresize = event.size
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				done = True
	
	if resize > 0 and time.time() > resize:
		size = sresize
		resize = 0
		screen = pygame.display.set_mode(size, flag)
		xmid = int(size[0]/2)
		ymid = int(size[1]/2)
		a = min(size)/4
		b = a
	
	screen.fill(BLACK)
	pygame.draw.circle(screen, GREY, (xmid,ymid), int(a+b), 1)
	
	mpos = pygame.mouse.get_pos()
	r = mpos[0]-xmid
	z = size[1]-mpos[1]-ymid
	
	d = math.sqrt(r**2 + z**2)
	if d != 0 and d <= a+b and r != 0 and d >= a-b:
		try:
			z1 = (a**2*z - b**2*z + r**2*z + z**3 + math.sqrt(r**2*(4*a**2*r**2 + 4*a**2*z**2 - (a**2 - b**2 + r**2 + z**2)**2)))/(2*(r**2 + z**2))
			r1 = math.sqrt(a**2 - z1**2)
			if abs(math.sqrt((r+r1)**2+(z-z1)**2) - b) < abs(math.sqrt((r-r1)**2+(z-z1)**2) - b):
				r1 = -r1
			
			#if (r < 0 and math.sqrt(r**2 + (z-(a+b)/2)**2) > (a+b)/2) ^ (r > 0 and math.sqrt(r**2 + (z-(a+b)/2)**2) < (a+b)/2):
			#	r1 = -r1
			
			pygame.draw.line(screen, WHITE, (xmid,ymid), (xmid+int(r1),ymid-int(z1)))
			pygame.draw.line(screen, WHITE, (xmid+int(r1),ymid-int(z1)), (xmid+r,ymid-z))
			screen.blit(font1.render("Ea= "+str(round(math.sqrt(r1**2+z1**2), 4)), False, BLACK, WHITE), (4,size[1]-80))
			screen.blit(font1.render("Eb= "+str(round(math.sqrt((r-r1)**2+(z-z1)**2), 4)), False, BLACK, WHITE), (4,size[1]-60))
			#if round(math.sqrt((r-r1)**2+(z-z1)**2), 4) != b:
			#	pygame.draw.circle(screen, RED, (xmid+r,ymid-z), 1, 1)
		except ValueError:
			pygame.draw.circle(screen, RED, (xmid+r,ymid-z), 1, 1)
		
	"""
	d = math.sqrt(r**2 + z**2)
	if d != 0 and d <= a+b and r != 0:
		alpha = math.acos((b**2-a**2-d**2) / (-2*a*d)) + math.atan2(z,r)
		try:
			beta = math.acos((d**2-b**2-a**2) / (-2*a*d)) + alpha - math.pi
		except ValueError:
			beta = 0
		screen.blit(font1.render(str(beta), False, BLACK, WHITE), (4,size[1]-60))
		
		ax = a*math.cos(alpha)
		ay = a*math.sin(alpha)
		bx = ax + b*math.cos(beta)
		by = ay + b*math.sin(beta)
		pygame.draw.line(screen, WHITE, (xmid,ymid), (xmid+ax,size[1]-ymid-ay))
		pygame.draw.line(screen, WHITE, (xmid+ax,size[1]-ymid-ay), (xmid+bx,size[1]-ymid-by))
	
		screen.blit(font1.render(str(math.sqrt((ax-bx)**2+(ay-by)**2)), False, BLACK, WHITE), (4,size[1]-40))"""
	screen.blit(font1.render(str((a,b)), False, BLACK, WHITE), (4,size[1]-40))
	screen.blit(font1.render(str((r,z)), False, BLACK, WHITE), (4,size[1]-20))
	
	pygame.display.flip()
	clock.tick(speed)

pygame.quit()
