#define pin_cnc_en 8
#define pin_cnc_m1_dir 5
#define pin_cnc_m2_dir 6
#define pin_cnc_m3_dir 7
#define pin_cnc_m1_stp 2
#define pin_cnc_m2_stp 3
#define pin_cnc_m3_stp 4

#define UI8  1
#define UI16 2
#define UI24 3
#define UI32 4
#define SI8  5
#define SI16 6
#define SI24 7
#define SI32 8
#define SF32 9
#define SF64 10
#define BV   11
#define BX8  12
#define BX16 13
#define BX24 14
#define BX32 15

#define BUFFER 32

float scale_x = 1.0;
float scale_y = 1.0;
float scale_z = 1.0;

unsigned char cmd;
unsigned char args[BUFFER];
unsigned char args_l;
unsigned char args_m;

/*
 * Bug: the controller crashes and all components heat a lot.
 * happens mainly when using very small values or zero for one of rx,ry,rz
 */
void step_xyz(int rx, int ry, int rz, unsigned long dur) {
  digitalWrite(pin_cnc_m1_dir, rx > 0);
  digitalWrite(pin_cnc_m2_dir, ry > 0);
  digitalWrite(pin_cnc_m3_dir, rz > 0);
  rx = abs(rx);
  ry = abs(ry);
  rz = abs(rz);
  dur *= 1000;
  unsigned long dx = dur;
  unsigned long dy = dur;
  unsigned long dz = dur;
  unsigned long nx = 0;
  unsigned long ny = 0;
  unsigned long nz = 0;
  unsigned long t = micros();
  if(rx != 0) dx /= rx; else nx = micros() + dur;
  if(ry != 0) dy /= ry; else ny = micros() + dur;
  if(rz != 0) dz /= rz; else nz = micros() + dur;
  while(rx+ry+rz > 0) {
    t = micros();
    if(nx < t) {
      digitalWrite(pin_cnc_m1_stp, HIGH);
      nx = t + dx;
      rx --;
    }
    if(ny < t) {
      digitalWrite(pin_cnc_m2_stp, HIGH);
      ny = t + dy;
      ry --;
    }
    if(nz < t) {
      digitalWrite(pin_cnc_m3_stp, HIGH);
      nz = t + dz;
      rz --;
    }
    delayMicroseconds(800);
    digitalWrite(pin_cnc_m1_stp, LOW);
    digitalWrite(pin_cnc_m2_stp, LOW);
    digitalWrite(pin_cnc_m3_stp, LOW);
  }
}

void step(int pin_dir, int pin_stp, int r, unsigned long dur) {
  if(r == 0) {
    delay(dur);
    return;
  }
  digitalWrite(pin_dir, r > 0);
  r = abs(r);
  dur *= 1000;
  unsigned long d = dur / r;
  unsigned long n = 0;
  unsigned long t;
  while(r > 0) {
    t = micros();
    if(n < t) {
      digitalWrite(pin_stp, LOW);
      delayMicroseconds(800);
      digitalWrite(pin_stp, HIGH);
      n = t + d;
      r --;
    }
  }
}

void cmd_w() {
  unsigned long dur;
  memcpy(&dur, args, 4);
  delay(dur);
}

void cmd_x() {
  unsigned int dur;
  float x;
  memcpy(&dur, args, 2);
  memcpy(&x, args+2, 4);
  step(pin_cnc_m1_dir, pin_cnc_m1_stp, x*scale_x, dur);
}

void cmd_y() {
  unsigned int dur;
  float y;
  memcpy(&dur, args, 2);
  memcpy(&y, args+2, 4);
  step(pin_cnc_m2_dir, pin_cnc_m2_stp, y*scale_y, dur);
}

void cmd_z() {
  unsigned int dur;
  float z;
  memcpy(&dur, args, 2);
  memcpy(&z, args+2, 4);
  step(pin_cnc_m3_dir, pin_cnc_m3_stp, z*scale_z, dur);
}

void cmd_xyz() {
  unsigned int dur;
  float x;
  float y;
  float z;
  memcpy(&dur, args, 2);
  memcpy(&x, args+2, 4);
  memcpy(&y, args+6, 4);
  memcpy(&z, args+10, 4);
  step_xyz(x*scale_x, y*scale_y, z*scale_z, dur);
}

void setup() {
  pinMode(pin_cnc_m1_dir, OUTPUT);
  pinMode(pin_cnc_m2_dir, OUTPUT);
  pinMode(pin_cnc_m3_dir, OUTPUT);
  pinMode(pin_cnc_m1_stp, OUTPUT);
  pinMode(pin_cnc_m2_stp, OUTPUT);
  pinMode(pin_cnc_m3_stp, OUTPUT);
  pinMode(pin_cnc_en, OUTPUT);
  digitalWrite(pin_cnc_en, HIGH);

  Serial.begin(9600);

  cmd = 0;
  args_l = 0;
}

void loop() {
  while(Serial.available() > 0) {
    unsigned char rec = Serial.read();
    if(cmd == 0) {
      cmd = rec;
      
      switch(cmd) {
        case 0x10: args_m = 4; break; // W
        case 0x11: args_m = 1; break; // EN
        case 0x12: args_m = 1; break; // POS
        case 0x13: args_m = 0; break; // HOME
        case 0x20: args_m = 6; break; // X
        case 0x21: args_m = 6; break; // Y
        case 0x22: args_m = 6; break; // Z
        case 0x23: args_m = 10; break; // XY
        case 0x24: args_m = 10; break; // XZ
        case 0x25: args_m = 10; break; // YZ
        case 0x26: args_m = 14; break; // XYZ
      }
    } else if(args_l < BUFFER) {
      args[args_l] = rec;
      args_l ++;
      
      if(args_l >= args_m) {
        switch(cmd) {
          case 0x10: cmd_w(); break; // W
          case 0X11: digitalWrite(pin_cnc_en, args[0] == 0); break; // EN
          case 0x20: cmd_x(); break; // X
          case 0x21: cmd_y(); break; // Y
          case 0x22: cmd_z(); break; // Z
          case 0x26: cmd_xyz(); break; // XYZ
          default:
            Serial.println(cmd);
        }
        
        cmd = 0;
        args_l = 0;
      }
    } else {
      cmd = 0;
      args_l = 0;
    }
  }
}
