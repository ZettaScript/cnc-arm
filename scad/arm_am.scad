use <motor_holder.scad>;
use <nema_stepper.scad>;

module ArmAM(
	as_h = 20, // AS height
	beam_as_dist = 18, // distance between the 2 bolts
	beam_as_l = 5, // AS beam slider length
	beam_as_m = 7, // AS beam marge
	beam_as_r = 1.75, // AS beam screw radius
	beam_as_th = 4, // AS beam thickness
	beam_as_w = 8, // AS beam width
	beam_mv_w = 8, // motor vertical beam width
	beam_s_h = 4, // side bottom beam height
	beam_s_m = 2, // side beam marge
	beam_s_th = 4, // side beam thickness
	beam_u_dist = 16, // distance between the 2 up beams
	beam_u_m = 7, // distance between center of bolt in upper position and up beam
	beam_u_th = 4, // up beam thickness
	beam_u_w = 8, //up beam width
	holder_th = 4,
	holder_w = 5.5,
	motor_h = 34,
	motor_screw_dist = 31,
	motor_w = 42.3,
	slider_l = 10,
) {
	beam_u_space = beam_u_m-(motor_w-motor_screw_dist)/2+slider_l/2;
	leg_th = 3;
	motor_border_mr = 2;
	
	// Holder
	rotate([0, 90, 0])
	translate([-slider_l/2 -motor_screw_dist/2, -motor_screw_dist/2, 0])
		MotorHolder(
			holder_mid=holder_w,
			holder_th=holder_th,
			beam_w = holder_th,
			motor_screw_dist=motor_screw_dist,
			motor_border_mr=motor_border_mr,
			slider_l=slider_l,
			leg_th=leg_th,
			$fn=15
		);
	
	// Motor vertical beam
	translate([0, motor_screw_dist/2+holder_w-beam_mv_w, motor_screw_dist/2+slider_l/2+motor_border_mr+leg_th])
		cube([holder_th, beam_mv_w, beam_u_m-motor_border_mr-leg_th+beam_u_th]);
	translate([0, -motor_screw_dist/2-holder_w, motor_screw_dist/2+slider_l/2+motor_border_mr+leg_th])
		cube([holder_th, beam_mv_w, beam_u_m-motor_border_mr-leg_th+beam_u_th]);
	
	translate([0, 0, motor_screw_dist/2+slider_l/2+beam_u_m]) {
		// Up beam
		translate([-motor_h/2-beam_u_dist, motor_screw_dist/2+holder_w-beam_mv_w, 0])
			cube([motor_h/2+beam_u_dist, beam_mv_w, beam_u_th]);
		translate([-motor_h/2-beam_u_dist, -motor_screw_dist/2-holder_w, 0])
			cube([motor_h/2+beam_u_dist, beam_mv_w, beam_u_th]);
		
		translate([-motor_h/2, -motor_screw_dist/2-holder_w-beam_s_m, 0]) {
			// Up beam
			translate([beam_u_dist/2, 0, 0])
				cube([beam_u_w, motor_screw_dist+2*holder_w+beam_s_m, beam_u_th]);
			translate([-beam_u_dist/2-beam_u_w, 0, 0])
				cube([beam_u_w, motor_screw_dist+2*holder_w+beam_s_m, beam_u_th]);
			
			// Side beam
			translate([0, -beam_s_th, -motor_w+as_h/2-beam_u_space]) {
				translate([beam_u_dist/2, 0, 0])
					cube([beam_u_w, beam_s_th, beam_u_th+motor_w-as_h/2+beam_u_space]);
				translate([-beam_u_dist/2-beam_u_w, 0, 0])
					cube([beam_u_w, beam_s_th, beam_u_th+motor_w-as_h/2+beam_u_space]);
				translate([-beam_u_dist/2, 0, 0])
					cube([beam_u_dist, beam_s_th, beam_s_h]);
				
				translate([0, -beam_as_m, 0]) {
					translate([beam_as_dist/2, 0, 0]) {
						rotate([0, 0, -90])
							Slider(beam_as_th, beam_as_l, beam_as_r, beam_as_w/2, $fn=20);
							difference() {
								translate([-beam_as_w/2, -beam_as_l/2, 0])
									cube([beam_as_w, beam_as_m+beam_as_l/2, beam_as_th]);
								translate([0,0,-1]) rotate([0, 0, -90])
									SliderHole(beam_as_th+2, beam_as_l, beam_as_r, $fn=20);
							}
					}
					translate([-beam_as_dist/2, 0, 0]) {
						rotate([0, 0, -90])
							Slider(beam_as_th, beam_as_l, beam_as_r, beam_as_w/2, $fn=20);
							difference() {
								translate([-beam_as_w/2, -beam_as_l/2, 0])
									cube([beam_as_w, beam_as_m+beam_as_l/2, beam_as_th]);
								translate([0,0,-1]) rotate([0, 0, -90])
									SliderHole(beam_as_th+2, beam_as_l, beam_as_r, $fn=20);
							}
					}
				}
			}
		}
	}
}

color([1,0,0,0.8]) rotate([0, 90, 0]) NemaStepper();
ArmAM();
