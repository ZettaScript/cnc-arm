module ArmLBCP(
	l = 600, // length (global b)
	c = 50, // global
	w = 10, // width
	t = 4, // thickness
	j1h_r = 2.25, // joint 1 hole radius
	j2h_r = 2.25 // joint 2 hole radius
) {
	difference() {
		hull() {
			translate([-c,0,0]) cylinder(d=w, h=t);
			translate([l,0,0]) cylinder(d=w, h=t);
		};
		
		translate([-c,0,-0.5]) cylinder(r=j2h_r, h=t+1, $fn=20);
		translate([0,0,-0.5]) cylinder(r=j1h_r, h=t+1, $fn=20);
		translate([l,0,-0.5]) cylinder(r=j2h_r, h=t+1, $fn=20);
	};
}

ArmLBCP(100, 50, 10, 4, 2.25, 2.25);
