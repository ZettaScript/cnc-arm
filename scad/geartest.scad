use <gears.scad>;

n1 = 17;
n2 = 86;
pa = 20;
ha = 20;
m = 1;

r1 = m*n1/2;
r2 = m*n2/2;

spur_gear(modul=m, tooth_number=n1, width=5, bore=5.5, pressure_angle=pa, helix_angle=ha, optimized=true);
translate([r1+r2,0,0]) rotate([0,0,180/n2]) spur_gear(modul=m, tooth_number=n2, width=5.5, bore=5, pressure_angle=pa, helix_angle=-ha, optimized=true, $fn=40);
