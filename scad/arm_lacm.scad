use <pitch.scad>;
use <gears.scad>;

module ArmLACM(
	c = 50, // length (global)
	a_a = 0, // arm angle
	w = 10, // width
	t = 4, // thickness
	mh_r = 2.25, // motor hole radius
	jh_r = 2.25, // pivot hole radius
	f_x = 35, // fork x
	f_s = 4.5, // fork free space
	f_t = 4, // fork thickness
	g_th = 6, // gear thickness
	g_n = 144, // gear teeth
	g_m = 1, // gear modul
	g_pa = 20, // gear pressure angle
	g_ha = 20, // gear helix angle
) {
	g_da = g_m * (g_n + ((g_m <1)? 2.2 : 2)); // Gear tip diameter
	
	translate([0,0,g_th]) {
		difference() {
			hull() {
				cylinder(d=w, h=2*t+f_s);
				translate([c,0,0]) cylinder(d=w, h=2*t+f_s);
			};
			
			translate([0,0,-0.5]) cylinder(d=w, h=2*t+f_s+1);
			translate([0,0,4.375]) rotate([0,90,90]) cylinder(h=w/2+1, r=1.625, $fn=20);
			
			translate([-w,-w/2-0.5,t]) cube([w+f_x,w+1,t+f_s+1]);
			translate([f_x+f_t,-w/2-0.5,t]) cube([c-f_x-f_t+w,w+1,f_s]);
			
			cylinder(r=mh_r, h=t);
			translate([c,0,-0.5]) cylinder(r=jh_r, h=2*t+f_s+1, $fn=20);
		};
		rotate([0,0,a_a]) translate([0,0,4.375]) rotate([0,0,90]) rod_pitch_m3(mh_r, w/2-mh_r);
	};
	rotate([0,0,a_a]) difference() {
		translate([0,0,g_th]) rotate([180,0,45]) spur_gear(modul=g_m, tooth_number=g_n, width=g_th, bore=mh_r*2, pressure_angle=g_pa, helix_angle=g_ha, optimized=true);
		difference() {
			translate([-g_da-w/2,-g_da/2,-0.5]) cube([g_da, g_da, g_th+1]);
			translate([0,0,-1]) cylinder(r=19, h=g_th+2);
		}
	}
}

ArmLACM();
