/*
 * This is only for testing.
 * For 3D-printing, please use files separately.
 */

use <arm_am.scad>;
use <arm_as.scad>;
use <arm_asw.scad>;
use <arm_bj.scad>;
use <arm_gm1.scad>;
use <arm_gma2.scad>;
use <arm_labf.scad>;
use <arm_labm.scad>;
use <arm_lacm.scad>;
use <arm_lacp.scad>;
use <arm_lbcf.scad>;
use <arm_lbcp.scad>;
use <arm_zs.scad>;
use <nema_stepper.scad>;

// Global constants
a = 400; // A length
b = 500; // B length
c = 50; // C length
sa = 40; // SA length
sb = 50; // SB length
gma1_n = 19; // GMA1 teeth
gma2_n = 144; // GMA2 teeth
gmb1_n = 19; // GMB1 teeth
gmb2_n = 144; // GMB2 teeth

as_am_screw_m = 16; // dist between AS-AM screw and AS border
as_h = 30; // AS height
as_l = 82; // AS length
as_w = 40; // AS width
axis_r = 4;
gma_m = 1; // GMA modul
gma2_th = 6; // GMA2 thickness
gmb_m = 1; // GMB modul
gmb2_th = 6; // GMB2 thickness
labf_th = 4; // LABF thickness
lacm_f_s = 4.5; // LACM fork free space
lacm_f_th = 4; // LACM fork thickness
lacm_th = 4; // LACM thickness
lacm_w = 10; // LACM width
lacp_th = 4; // LACP thickness
stepper_ch = 2; // Stepper base cylinder height
stepper_h = 34; // Stepper height
stepper_l = 22; // Stepper rod length
stepper_w = 42.3; // Stepper width

// Demo constants
alpha = 60;
beta = 30;
as_altitude = 110;
axis_h = 160;
lacm_dist = 0;
lab_dist = 0;

// Computed constants
gma_dist = gma_m*gma2_n/2 + gma_m*gma1_n/2;
gma_ydist = -stepper_w/2-as_h/2;
gma_xdist = sqrt(pow(gma_dist, 2) - pow(gma_ydist, 2));
gma_rel_angle = asin(gma_ydist / gma_dist);
gmb_dist = gmb_m*gmb2_n/2 + gmb_m*gmb1_n/2;
gmb_ydist = stepper_w/2+as_h/2;
gmb_xdist = sqrt(pow(gmb_dist, 2) - pow(gmb_ydist, 2));
gmb_rel_angle = asin(gmb_ydist / gmb_dist);

ArmZS(axis_r=axis_r);

color([1,0,0,0.8]) cylinder(r=axis_r, h=axis_h);

translate([0, 0, as_altitude]) {
	ArmAS(sa=sa, am_screw_m=as_am_screw_m, base_h=as_h, base_l=as_l, base_w=as_w, vaxis_r=axis_r);
	
	translate([0, 0, as_h]) ArmASW(r=axis_r);
	rotate([0, 180, 0]) ArmASW(r=axis_r);
	
	translate([sa/2, -as_w/2-lacm_dist/*-lacm_th-lacm_f_s-lacm_f_th-gmb2_th*/-labf_th, as_h/2]) rotate([90, 180+beta, 0]) ArmLACM(c=c, a_a=gmb_rel_angle, t=lacm_th, g_n=gmb2_n, w=lacm_w, f_s=lacm_f_s, f_t=lacm_f_th);
	//translate([sa/2-c*cos(beta), -as_w/2-lacm_dist-(lacm_f_s-lacp_th)/2-lacm_f_th-labf_th, as_h/2+c*sin(beta)]) rotate([90, -alpha, 0]) ArmLACP(l=a, t=lacp_th);
	translate([sa/2-c*cos(beta), -as_w/2-lacm_dist-labf_th-gmb2_th-lacm_th-(lacm_f_s-lacp_th)/2, as_h/2+c*sin(beta)]) rotate([90, -alpha, 0]) ArmLACP(l=a, t=lacp_th);
	
	translate([sa/2, -as_w/2-lab_dist, as_h/2]) rotate([90, -alpha, 0]) ArmLABM(l=a);
	translate([sa/2, as_w/2+lab_dist, as_h/2]) rotate([-90, -alpha, 0]) ArmLABM(l=a);
	
	translate([sa/2, as_w/2+lab_dist+labf_th, as_h/2]) rotate([-90, 180-alpha-gma_rel_angle, 0]) ArmGMA2(g_n=gma2_n);
	
	translate([-sa/2, -as_w/2-lab_dist, as_h/2]) rotate([90, -alpha, 0]) ArmLABF(l=a, t=labf_th);
	translate([-sa/2, as_w/2+lab_dist, as_h/2]) rotate([-90, -alpha, 0]) ArmLABF(l=a, t=labf_th);
	
	translate([-sa/2+a*cos(alpha), 6, as_h/2+a*sin(alpha)]) rotate([90, 0, 0]) ArmBJ(sa=sa, sb=sb);
	
	translate([sa/2+a*cos(alpha), 2, as_h/2+a*sin(alpha)]) rotate([90, beta, 0]) ArmLBCP(l=b);
	
	translate([sa/2+a*cos(alpha), 2, as_h/2+a*sin(alpha)+sb]) rotate([90, beta, 0]) ArmLBCF(l=b);
	
	translate([sa/2-gma_xdist, 0, as_h/2-gma_ydist]) {
		//translate([0,as_w/2-stepper_ch,0]) {
		translate([0,stepper_h/2,0]) {
			rotate([90,0,180]) color([1,0,0,0.8]) NemaStepper(l=stepper_l, ch=stepper_ch, h=stepper_h, w=stepper_w);
			//rotate([0,0,90]) ArmAM(as_h=as_h, motor_h=stepper_h, motor_w=stepper_w);
		}
		translate([0, as_w/2+labf_th, 0]) rotate([-90,0,0]) rotate([0,0,180+alpha*gma2_n/gma1_n+180/gma1_n//+TODO ajustement dent
		]) ArmGM1(teeth_nb=gma1_n);
		
	}
	
	
	/*translate([sa/2-gmb_m*gmb2_n/2-gmb_m*gmb1_n/2,-as_w/2-lacm_dist-lacm_th-lacm_f_s-lacm_f_th-labf_th-stepper_l,as_h/2]) rotate([-90,0,0]) {
		color([1,0,0,0.8]) NemaStepper(l=stepper_l, ch=stepper_ch);
		translate([0,0,stepper_l]) rotate([180,0,180/gmb1_n]) Armgmb(teeth_nb=gmb1_n);
	};*/
	translate([sa/2-gmb_xdist, 0, as_h/2-gmb_ydist]) {
		translate([0,-stepper_h/2,0]) {
			rotate([-90,0,180]) color([1,0,0,0.8]) NemaStepper(l=stepper_l, ch=stepper_ch, h=stepper_h, w=stepper_w);
			//rotate([0,180,90]) ArmAM(as_h=as_h, motor_h=stepper_h, motor_w=stepper_w);
		}
		translate([0, -as_w/2-lacm_dist/*-lacm_th-lacm_f_s-lacm_f_th-gmb2_th*/-labf_th, 0]) rotate([90,0,0]) rotate([0,0,180+(beta+gmb_rel_angle)*gmb2_n/gmb1_n+180/gmb1_n/*+TODO ajustement dent*/]) ArmGM1(teeth_nb=gmb1_n);
	}
}
