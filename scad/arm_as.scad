/*
 * CopyLeft 2019-2020 Pascal Engélibert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use <pitch.scad>;

module RodPitch(
	vaxis_margin,
	vaxis_r,
	base_h,
	rod_c_th,
	rod_screw_dist,
	wedge_th,
) {
	difference() {
		union() {
			translate([vaxis_r+vaxis_margin+rod_c_th+wedge_th, 0, rod_screw_dist])
				rotate([90, 0, 90])
					simple_pitch_m3(2, 2, vaxis_r+vaxis_margin);
			hull() {
				cylinder(r=vaxis_r+vaxis_margin+rod_c_th, h=rod_screw_dist+6, $fn=40);
				translate([wedge_th, 0, 0])
					cylinder(r=vaxis_r+vaxis_margin+rod_c_th, h=rod_screw_dist+6, $fn=40);
			}
		}
		hull() {
			cylinder(r=vaxis_r+vaxis_margin, h=rod_screw_dist+7, $fn=40);
			translate([wedge_th, 0, 0])
				cylinder(r=vaxis_r+vaxis_margin, h=rod_screw_dist+7, $fn=40);
		}
		translate([0, 0, rod_screw_dist]) rotate([0, 90, 0])
			cylinder(r=M3_RAY(), h=vaxis_r+vaxis_margin+rod_c_th+wedge_th, $fn=20);
	}
}

module ArmAS(
	sa = 60, // global
	am_screw_dist = 18,
	am_screw_l = 3,
	am_screw_m = 24,
	base_l = 82,
	base_w = 40,
	base_h = 30,
	bearing_dist = 3,
	bearing_margin_r = 0.5,
	bearing_margin_th = 0.5,
	bearing_r = 11,
	bearing_th = 7.5,
	haxis_r = 4,
	haxis_margin = 1,
	rod_c_th = 3,
	rod_screw_dist = 5,
	screw_l = 2,
	vaxis_r = 4,
	vaxis_margin = 1,
	wedge_th = 2,
) {
	translate([-base_l/2, -base_w/2, 0]) difference() {
		cube([base_l, base_w, base_h]);
		
		translate([base_l/2, base_w/2, -0.5]) cylinder(r=vaxis_r+vaxis_margin, h=base_h+1, $fn=40);
		
		translate([(base_l-sa)/2, base_w/2, base_h/2]) rotate([90, 0, 0]) {
			cylinder(r=haxis_r+haxis_margin, h=base_w+2, center=true, $fn=40);
			translate([0, 0, base_w/2-(bearing_th+bearing_margin_th)/2-bearing_dist]) cylinder(r=bearing_r+bearing_margin_r, h=bearing_th+bearing_margin_th, center=true, $fn=60);
			translate([0, 0, -base_w/2+(bearing_th+bearing_margin_th)/2+bearing_dist]) cylinder(r=bearing_r+bearing_margin_r, h=bearing_th+bearing_margin_th, center=true, $fn=60);
		}
		translate([(base_l+sa)/2, base_w/2, base_h/2]) rotate([90, 0, 0]) {
			cylinder(r=haxis_r+haxis_margin, h=base_w+2, center=true, $fn=40);
			translate([0, 0, base_w/2-(bearing_th+bearing_margin_th)/2-bearing_dist]) cylinder(r=bearing_r+bearing_margin_r, h=bearing_th+bearing_margin_th, center=true, $fn=60);
			translate([0, 0, -base_w/2+(bearing_th+bearing_margin_th)/2+bearing_dist]) cylinder(r=bearing_r+bearing_margin_r, h=bearing_th+bearing_margin_th, center=true, $fn=60);
		}
		
		if(bearing_dist > 0) {
			translate([-1, bearing_dist, base_h/2-bearing_r-bearing_margin_r]) cube([(base_l-sa)/2+1, bearing_th+bearing_margin_th, 2*(bearing_r+bearing_margin_r)]);
			translate([-1, base_w-bearing_dist-bearing_th-bearing_margin_th, base_h/2-bearing_r-bearing_margin_r]) cube([(base_l-sa)/2+1, bearing_th+bearing_margin_th, 2*(bearing_r+bearing_margin_r)]);
			translate([base_l-bearing_r-bearing_margin_r, bearing_dist, base_h/2-bearing_r-bearing_margin_r]) cube([(base_l-sa)/2+1, bearing_th+bearing_margin_th, 2*(bearing_r+bearing_margin_r)]);
			translate([base_l-bearing_r-bearing_margin_r, base_w-bearing_dist-bearing_th-bearing_margin_th, base_h/2-bearing_r-bearing_margin_r]) cube([(base_l-sa)/2+1, bearing_th+bearing_margin_th, 2*(bearing_r+bearing_margin_r)]);
			
			translate([3, bearing_dist+(bearing_th+bearing_margin_th)/2, base_h/2+bearing_r+bearing_margin_r-3]) pitch_inv_m3(0, base_h/2-bearing_r-bearing_margin_r, 0);
			translate([3, base_w-bearing_dist-(bearing_th+bearing_margin_th)/2, base_h/2+bearing_r+bearing_margin_r-3]) pitch_inv_m3(0, base_h/2-bearing_r-bearing_margin_r, 0);
			translate([base_l-3, bearing_dist+(bearing_th+bearing_margin_th)/2, base_h/2+bearing_r+bearing_margin_r-3]) pitch_inv_m3(0, base_h/2-bearing_r-bearing_margin_r, 0);
			translate([base_l-3, base_w-bearing_dist-(bearing_th+bearing_margin_th)/2, base_h/2+bearing_r+bearing_margin_r-3]) pitch_inv_m3(0, base_h/2-bearing_r-bearing_margin_r, 0);
		}
		
		translate([am_screw_m, base_w/2, 0]) {
			translate([0, am_screw_dist/2, am_screw_l])
				pitch_inv_m3(base_w/2-am_screw_dist/2+1, base_h, am_screw_l);
			translate([0, am_screw_dist/2, base_h-am_screw_l]) rotate([0, 180, 0])
				pitch_inv_m3(base_w/2-am_screw_dist/2+1, base_h, am_screw_l);
			translate([0, -am_screw_dist/2, am_screw_l]) rotate([0, 0, 180])
				pitch_inv_m3(base_w/2-am_screw_dist/2+1, base_h, am_screw_l);
			translate([0, -am_screw_dist/2, base_h-am_screw_l]) rotate([180, 0, 0])
				pitch_inv_m3(base_w/2-am_screw_dist/2+1, base_h, am_screw_l);
		}
	}
	translate([0, 0, base_h])
		RodPitch(vaxis_margin, vaxis_r, base_h, rod_c_th, rod_screw_dist, wedge_th);
	rotate([180, 0, 0])
		RodPitch(vaxis_margin, vaxis_r, base_h, rod_c_th, rod_screw_dist, wedge_th);
}

ArmAS();
/*difference() {
	ArmAS(base_l=56, am_screw_m=14);
	translate([-8, -50, -50]) cube([100, 100, 100]);
	translate([-100, -100, -50]) cube([100, 100, 100]);
}*/
