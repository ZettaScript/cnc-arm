use <motor_holder.scad>;

// Default parameters good for NEMA 17 stepper
module ArmZM(
	holder_th = 4,
	holder_mid = 8,
	motor_screw_dist = 31,
	motor_screw_r = 2.1,
	motor_screw_mr = 4.5,
	motor_border_mr = 6,
	slider_l = 15,
	leg_th = 5,
	leg_l = 45,
	foot_th = 4,
	foot_screw_r = 2.2,
	foot_screw_mr = 6,
	beam_w = 5
) {
	translate([0,0,leg_l]) MotorHolder(holder_th, holder_mid, motor_screw_dist, motor_screw_r, motor_screw_mr, motor_border_mr, slider_l, leg_th, beam_w);
	
	// legs
	translate([-motor_border_mr-leg_th,-holder_mid,0]) cube([leg_th, holder_mid, leg_l]);
	translate([motor_screw_dist+slider_l+motor_border_mr,-holder_mid,0]) cube([leg_th, holder_mid, leg_l]);
	translate([-motor_border_mr-leg_th,motor_screw_dist,0]) cube([leg_th, holder_mid, leg_l]);
	translate([motor_screw_dist+slider_l+motor_border_mr,motor_screw_dist,0]) cube([leg_th, holder_mid, leg_l]);
	
	// feet
	translate([-motor_border_mr-leg_th-2*foot_screw_mr,-foot_screw_mr-holder_mid/2,0]) difference() {
		cube([2*foot_screw_mr,2*foot_screw_mr,foot_th]);
		translate([foot_screw_mr,foot_screw_mr,0]) cylinder(r=foot_screw_r, h=foot_th);
	}
	translate([motor_screw_dist+slider_l+motor_border_mr+leg_th,-foot_screw_mr-holder_mid/2,0]) difference() {
		cube([2*foot_screw_mr,2*foot_screw_mr,foot_th]);
		translate([foot_screw_mr,foot_screw_mr,0]) cylinder(r=foot_screw_r, h=foot_th);
	}
	translate([-motor_border_mr-leg_th-2*foot_screw_mr,motor_screw_dist-foot_screw_mr+holder_mid/2,0]) difference() {
		cube([2*foot_screw_mr,2*foot_screw_mr,foot_th]);
		translate([foot_screw_mr,foot_screw_mr,0]) cylinder(r=foot_screw_r, h=foot_th);
	}
	translate([motor_screw_dist+slider_l+motor_border_mr+leg_th,motor_screw_dist-foot_screw_mr+holder_mid/2,0]) difference() {
		cube([2*foot_screw_mr,2*foot_screw_mr,foot_th]);
		translate([foot_screw_mr,foot_screw_mr,0]) cylinder(r=foot_screw_r, h=foot_th);
	}
	
	// beams
	translate([-motor_border_mr-leg_th,0,0]) cube([leg_th,motor_screw_dist,beam_w]);
	translate([motor_screw_dist+slider_l+motor_border_mr,0,0]) cube([leg_th,motor_screw_dist,beam_w]);
}

ArmZM();
