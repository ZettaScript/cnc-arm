module NemaStepper(
	h = 34, // height
	w = 42.3, // width
	l = 22, // rod length (incl base cylinder)
	r = 2.5, // rod radius
	cr = 11, // base cylinder radius
	ch = 2, // base cylinder height
	sd = 15.5, // screw holes distance
	sr = 1.5 // screw holes radius
) {
	translate([0,0,-h/2]) difference() {
		cube([w,w,h], center=true);
		translate([sd,sd,0]) cylinder(r=sr, h=h+1, center=true, $fn=20);
		translate([sd,-sd,0]) cylinder(r=sr, h=h+1, center=true, $fn=20);
		translate([-sd,sd,0]) cylinder(r=sr, h=h+1, center=true, $fn=20);
		translate([-sd,-sd,0]) cylinder(r=sr, h=h+1, center=true, $fn=20);
	}
	cylinder(r=cr, h=ch);
	cylinder(r=r, h=l, $fn=30);
}

NemaStepper();