module ArmZS(
	side_th = 3,
	bearing_r = 11,
	bearing_th = 7.5,
	bearing_margin_w = 0.5,
	bearing_margin_h = 0.5,
	axis_r = 4,
	axis_margin = 2,
	bearings_d = 30,
	fix_th = 4,
	fix_l = 10,
	fix_bolt_r = 2,
	fix_r = 6,
	fix_buttress_th = 3,
	fix_buttress_h = 50
) {
	difference() {
		cylinder(r=bearing_r+bearing_margin_w+side_th, h=bearing_th+bearing_margin_h);
		
		translate([0,0,-0.5]) cylinder(r=bearing_r+bearing_margin_w, h=bearing_th+bearing_margin_h+1, $fn=60);
	}
	translate([0,0,bearing_th]) difference() {
		cylinder(r=bearing_r+bearing_margin_w+side_th, h=bearings_d);
		
		translate([0,0,-0.5]) cylinder(r=axis_r+axis_margin, h=bearings_d+1, $fn=40);
	}
	translate([0,0,bearing_th+bearings_d]) difference() {
		cylinder(r=bearing_r+bearing_margin_w+side_th, h=bearing_th+bearing_margin_h);
		
		translate([0,0,-0.5]) cylinder(r=bearing_r+bearing_margin_w, h=bearing_th+bearing_margin_h+1, $fn=60);
	}
	
	difference() {
		union() {
			difference() {
				hull() {
					translate([bearing_r+bearing_margin_w+side_th+fix_l+fix_bolt_r,0,0]) cylinder(r=fix_r, h=fix_th);
					translate([-(bearing_r+bearing_margin_w+side_th+fix_l+fix_bolt_r),0,0]) cylinder(r=fix_r, h=fix_th);
				}
				translate([bearing_r+bearing_margin_w+side_th+fix_l+fix_bolt_r,0,-0.5]) cylinder(r=fix_bolt_r, h=fix_th+1, $fn=20);
				translate([-(bearing_r+bearing_margin_w+side_th+fix_l+fix_bolt_r),0,-0.5]) cylinder(r=fix_bolt_r, h=fix_th+1, $fn=20);
			}
			difference() {
				hull() {
					translate([0,bearing_r+bearing_margin_w+side_th+fix_l+fix_bolt_r,0]) cylinder(r=fix_r, h=fix_th);
					translate([0,-(bearing_r+bearing_margin_w+side_th+fix_l+fix_bolt_r),0]) cylinder(r=fix_r, h=fix_th);
				}
				translate([0,bearing_r+bearing_margin_w+side_th+fix_l+fix_bolt_r,-0.5]) cylinder(r=fix_bolt_r, h=fix_th+1, $fn=20);
				translate([0,-(bearing_r+bearing_margin_w+side_th+fix_l+fix_bolt_r),-0.5]) cylinder(r=fix_bolt_r, h=fix_th+1, $fn=20);
			}
			hull() {
				translate([bearing_r+bearing_margin_w+side_th+fix_l+fix_bolt_r-fix_r-fix_buttress_th,0,0]) cylinder(r=fix_buttress_th, h=fix_th);
				translate([-(bearing_r+bearing_margin_w+side_th+fix_l+fix_bolt_r-fix_r-fix_buttress_th),0,0]) cylinder(r=fix_buttress_th, h=fix_th);
				cylinder(r=fix_buttress_th, h=fix_buttress_h);
			}
			hull() {
				translate([0,bearing_r+bearing_margin_w+side_th+fix_l+fix_bolt_r-fix_r-fix_buttress_th,0]) cylinder(r=fix_buttress_th, h=fix_th);
				translate([0,-(bearing_r+bearing_margin_w+side_th+fix_l+fix_bolt_r-fix_r-fix_buttress_th),0]) cylinder(r=fix_buttress_th, h=fix_th);
				cylinder(r=fix_buttress_th, h=fix_buttress_h);
			}
		}
		translate([0,0,-0.5]) cylinder(r=bearing_r+bearing_margin_w, h=2*(bearing_th+bearing_margin_h)+bearings_d+fix_buttress_h+1, $fn=60);
	}
}

ArmZS();