module ArmBJ(
	sa = 40, // sa (global)
	sb = 50, // sb (global)
	tt = 12, // total thickness
	m_r = 5, // marge radius
	f_t = 4.5, // free thickness
	jh_r = 2.25 // hole radius
) {
	difference() {
		union() {
			hull() {
				cylinder(r=m_r, h=tt);
				translate([sa,0,0]) cylinder(r=m_r, h=tt);
			};
			hull() {
				cylinder(r=m_r, h=tt);
				translate([sa,sb,0]) cylinder(r=m_r, h=tt);
			};
			hull() {
				translate([sa,0,0]) cylinder(r=m_r, h=tt);
				translate([sa,sb,0]) cylinder(r=m_r, h=tt);
			};
			hull() {
				translate([sa,sb,0]) cylinder(r=m_r, h=tt);
				translate([sa,sb+m_r*2,0]) cylinder(r=m_r, h=tt);
			};
		};
		
		translate([0,0,-0.5]) cylinder(r=jh_r, h=tt+1, $fn=20);
		translate([sa,0,-0.5]) cylinder(r=jh_r, h=tt+1, $fn=20);
		translate([sa,sb,-0.5]) cylinder(r=jh_r, h=tt+1, $fn=20);
		
		translate([-m_r,-m_r-1,(tt-f_t)/2]) cube([sa+2*m_r+1,sb+2*m_r+1,f_t]);
	};
}

ArmBJ(40, 50, 12, 5, 4.5, 2.25);