module SliderHole(th, l, r) {
	hull() {
		cylinder(r=r, h=th);
		translate([l,0,0]) cylinder(r=r, h=th);
	}
}

module Slider(th, l, r, mr) {
	difference() {
		hull() {
			cylinder(r=mr, h=th);
			translate([l,0,0]) cylinder(r=mr, h=th);
		}
		translate([0,0,-1]) SliderHole(th+2, l, r);
	}
}

// Default parameters good for NEMA 17 stepper
module MotorHolder(
	holder_th = 4,
	holder_mid = 8,
	motor_screw_dist = 31,
	motor_screw_r = 2,
	motor_screw_mr = 4.5,
	motor_border_mr = 6,
	slider_l = 15,
	leg_th = 5,
	beam_w = 4
) {
	// sliders
	Slider(holder_th, slider_l, motor_screw_r, motor_screw_mr);
	translate([motor_screw_dist,0,0]) Slider(holder_th, slider_l, motor_screw_r, motor_screw_mr);
	translate([0,motor_screw_dist,0]) Slider(holder_th, slider_l, motor_screw_r, motor_screw_mr);
	translate([motor_screw_dist,motor_screw_dist,0]) Slider(holder_th, slider_l, motor_screw_r, motor_screw_mr);
	
	// middle
	difference() {
		translate([-motor_border_mr-leg_th,-holder_mid,0]) cube([motor_screw_dist+slider_l+2*motor_border_mr+2*leg_th,holder_mid,holder_th]);
		translate([0,0,-1]) SliderHole(holder_th+2, slider_l, motor_screw_r);
		translate([motor_screw_dist,0,-1]) SliderHole(holder_th+2, slider_l, motor_screw_r);
	}
	translate([0,motor_screw_dist,0]) difference() {
		translate([-motor_border_mr-leg_th,0,0]) cube([motor_screw_dist+slider_l+2*motor_border_mr+2*leg_th,holder_mid,holder_th]);
		translate([0,0,-1]) SliderHole(holder_th+2, slider_l, motor_screw_r);
		translate([motor_screw_dist,0,-1]) SliderHole(holder_th+2, slider_l, motor_screw_r);
	}
	
	// beams
	translate([-motor_border_mr-leg_th,0,holder_th-beam_w]) cube([leg_th,motor_screw_dist,beam_w]);
	translate([motor_screw_dist+slider_l+motor_border_mr,0,holder_th-beam_w]) cube([leg_th,motor_screw_dist,beam_w]);
}

MotorHolder();
