use <gears.scad>;

module ArmGMA2(
	mh_r = 2.25, // motor hole radius
	g_th = 6, // gear thickness
	g_n = 86, // gear teeth
	g_m = 1, // gear modul
	g_pa = 20, // gear pressure angle
	g_ha = 20, // gear helix angle
) {
	g_da = g_m * (g_n + ((g_m <1)? 2.2 : 2)); // Gear tip diameter
	
	intersection() {
		translate([0,0,g_th]) rotate([180,0,45]) spur_gear(modul=g_m, tooth_number=g_n, width=g_th, bore=mh_r*2, pressure_angle=g_pa, helix_angle=g_ha, optimized=true);
		
		rotate([0,0,0]) union() {
			translate([-8,-8,-1]) cube([g_da+9, g_da+9, g_th+2]);
			translate([0,0,-1]) cylinder(r=19, h=g_th+2);
		}
	}
}

ArmGMA2(g_n=144, $fn=60);
