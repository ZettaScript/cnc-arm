/*
 * CopyLeft 2020 Pascal Engélibert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

module ArmASW(
	cut = 2,
	h = 13,
	r = 4,
	th = 2,
) {
	difference() {
		hull() {
			cylinder(r=r, h=h, $fn=40);
			translate([th, 0, 0]) cylinder(r=r, h=h, $fn=40);
		}
		cylinder(r=r, h=h+1, $fn=40);
		translate([0, -r, 0]) cube([cut, 2*r, h]);
	}
}

ArmASW();
