use <gears.scad>;
use <pitch.scad>;

module ArmGM1(teeth_nb=17, bore=5.5, helix_angle=-20, th=8, modul=1, mr=5, mh=5) {
	spur_gear(modul=modul, tooth_number=teeth_nb, width=th, bore=bore, pressure_angle=20, helix_angle=helix_angle, optimized=true);
	/*translate([0,0,th]) difference() {
		cylinder(r=mr, h=mh);
		cylinder(r=bore/2, h=mh);
	}*/
	translate([0,0,th]) rod_pitch_m3(bore/2, mr, false);
}

ArmGM1($fn=20);
