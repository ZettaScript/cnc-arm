module ArmLABF(
	l = 400, // length (global a)
	w = 10, // width
	t = 4, // thickness
	j1h_r = 2.25, // joint 1 hole radius
	j2h_r = 2.25 // joint 2 hole radius
) {
	difference() {
		hull() {
			cylinder(d=w, h=t);
			translate([l,0,0]) cylinder(d=w, h=t);
		};
		
		translate([0,0,-0.5]) cylinder(r=j1h_r, h=t+1, $fn=20);
		translate([l,0,-0.5]) cylinder(r=j2h_r, h=t+1, $fn=20);
	};
}

ArmLABF(100, 10, 4, 2.25, 2.25);
