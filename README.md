# CNC arm

This project is about making a CNC arm with 3 stepper motors and modular tools at the end (mill, LASER cutter, paintbrush, etc.). The tool can completely move in 3 directions, like with a common 3-axis CNC. The advantage is that it's easier to enlarge the accessible surface.

![](images/global.png)
![](images/body-a.png)
![](images/body-b.png)

**Nothing is functionnal _yet_! Please wait (or contribute :) )...**

## Pieces

Each 3D-printed piece has a unique codename:

| code | name |
| ---- | ---- |
| am | A motor holder |
| as | A base |
| asw | A base rod wedge |
| bj | B joint |
| bm | B motor holder |
| ce | C end |
| gma | A motor gear |
| gmb | B motor gear |
| labf | A-B follower arm |
| labm | A-B motor arm |
| lacm | A-C motor arm |
| lacp | A-C plus arm |
| lbcf | B-C follower arm |
| lbcp | B-C plus arm |
| zm | Z-axis motor holder |
| zs | Z-axis base |

## Software

### Driver

Like a 3d-print slicer, the driver transforms a file (image, path, 3d model...) into another file understood by the CNC machine. Mathematical computations are necessary to transform x,y,z coordinates into θ,α,β coordinates.

### Protocol

Generally, CNC machines read [G-code](https://en.wikipedia.org/wiki/G-code), but this language is awful (ASCII). So I'm using another language, called temporarily Gbincode, which is simply a binary G-code.

**Syntax**: 1 byte for command, _x_ bytes for arguments, ...

| Type code | C type | Size (bytes) |
| --- | --- | --- |
| ui8 | `unsigned char` | 1 |
| ui16 | `unsigned short` (`unsigned int` on Arduino) | 2 |
| ui24 | `unsigned long` | 3 |
| ui32 | `unsigned long` | 4 |
| si8 | `char` | 1 |
| si16 | `short` (`int` on Arduino) | 2 |
| si24 | `long` | 3 |
| si32 | `long` | 4 |
| sf32 | `float` | 4 |
| sf64 | `double` (only Arduino Due) | 8 |
| bv | `char*` | variable (end with `0x00`) |
| bx8 | `char[len]` | preceded by size (_ui8_ len) |
| bx16 | `char[len]` | preceded by size (_ui16_ len) |
| bx24 | `char[len]` | preceded by size (_ui24_ len) |
| bx32 | `char[len]` | preceded by size (_ui32_ len) |

Endianness depends of controller's CPU. Most are big endian (ATMega included).

| Name | Command | Arguments | Description |
| --- | --- | --- | --- |
| w | 0x10 | _ui32_ **duration** | Wait `duration` ms |
| en | 0x11 | _ui8_ **bool** | Enable/disable motors |
| pos | 0x12 | _ui8_ **bool** | Set movement mode: 0=absolute; 1=relative |
| home | 0x13 | | Go to home position |
| x | 0x20 | _ui16_ **duration**, _sf32_ **x** | motor 1 movement |
| y | 0x21 | _ui16_ **duration**, _sf32_ **y** | motor 2 movement |
| z | 0x22 | _ui16_ **duration**, _sf32_ **z** | motor 3 movement |
| xy | 0x23 | _ui16_ **duration**, _sf32_ **x**, _sf32_ **x** | motors 1,2 movement |
| xz | 0x24 | _ui16_ **duration**, _sf32_ **x**, _sf32_ **z** | motors 1,3 movement |
| yz | 0x25 | _ui16_ **duration**, _sf32_ **y**, _sf32_ **z** | motors 2,3 movement |
| xyz | 0x26 | _ui16_ **duration**, _sf32_ **x**, _sf32_ **y**, _sf32_ **z** | motors 1,2,3 movement |
| beep | 0x40 | _ui16_ **duration**, _ui16_ **freq** | Beep at `freq` Hz during `duration` ms |
| disp | 0x41 | _ui16_ **x**, _ui16_ **y**, _bx_ **text** | Display `text` at position (`x`,`y`) |

### Arduino program

## Make

Do not forget to set at least `$fn=60` when exporting OpenSCAD files.

Since somes pieces (LABM, LABF, LACP, LBCP, LBCF) are too wide for 3D-printing, they could be wooden.

## License

GNU AGPL 3.0  
CopyLeft 2019-2020 Pascal Engélibert

This project is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, version 3 of the License.

This project is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this project.  If not, see <https://www.gnu.org/licenses/>.
